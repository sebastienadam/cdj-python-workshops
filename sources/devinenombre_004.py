#!/usr/bin/env python3

import random

nombre_max = 15
essais = 0

print('Devine le nombre')

nombre = random.randint(1, nombre_max)

while True:
    print()
    essais += 1
    while True:
        devine = input(f"Ta proposition entre 1 et {nombre_max}: ")
        if devine.isnumeric():
            break
        print('Tu dois choisir un nombre entier')
    devine = int(devine)
    if nombre == devine:
        print(f'Bravo! tu as bien deviné en {essais} essais.')
        break
    elif nombre < devine:
        print('Trop haut, choisi un nombre plus petit')
    else:
        print('Trop bas, choisi un nombre plus grand')

