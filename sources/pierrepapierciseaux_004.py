#!/usr/bin/env python3

import random

valeurs = ['pierre', 'papier', 'ciseaux']
texte_ordinateur_gagne = "--- J'ai gagné ---"
texte_joueur_gagne = "+++ Tu a gagné +++"
texte_match_nul = '=== Match nul! ==='

print(f'Jeu "{" - ".join(valeurs)}"')

print()
ordinateur = random.choice(valeurs)
while True:
    joueur = input(f'Ton choix {valeurs}: ').lower()
    if joueur in valeurs:
        break
    print(f'Tu dois choisir entre: {" - ".join(valeurs)}')
print(f"J'ai choisi '{ordinateur}'")
if joueur == ordinateur:
    print(texte_match_nul)
elif joueur == 'pierre' and ordinateur == 'ciseaux':
    print(texte_joueur_gagne)
elif joueur == 'ciseaux' and ordinateur == 'papier':
    print(texte_joueur_gagne)
elif joueur == 'papier' and ordinateur == 'pierre':
    print(texte_joueur_gagne)
else:
    print(texte_ordinateur_gagne)
