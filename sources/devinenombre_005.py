#!/usr/bin/env python3

import random

nombre_max = 15
essais = 0

print('Devine le nombre')

while True:
    nombre = random.randint(1, nombre_max)
    while True:
        print()
        essais += 1
        while True:
            devine = input(f"Ton nombre entre 1 et {nombre_max}: ")
            if devine.isnumeric() or devine == '':
                break
            print('Tu dois choisir un nombre entier')
        if devine == '':
            break
        devine = int(devine)
        if nombre == devine:
            print(f'Bravo! tu as bien deviné en {essais} essais.')
            essais = 0
            break
        elif nombre < devine:
            print('Trop haut, choisi un nombre plus petit')
        elif nombre > devine:
            print('Trop bas, choisi un nombre plus grand')
    if devine == '':
        break
    print()
    print('Nouvelle partie')

print()
print('Au revoir')
