#!/usr/bin/env python3

from pathlib import Path
import random

# Chargement des mots
mots = Path('liste_mots.txt').read_text()
mots = mots.split('\n')

# Initialisation de la partie
mot = random.choice(mots)
lettres_proposees = []

# Affichage du mot à trouver
lettres_affichees = []
for lettre in mot:
    if lettre in lettres_proposees:
        lettres_affichees.append(lettre)
    else:
        lettres_affichees.append('_')
print(' '.join(lettres_affichees))
