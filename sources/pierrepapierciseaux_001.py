#!/usr/bin/env python3

import random

valeurs = ['pierre', 'papier', 'ciseaux']

ordinateur = random.choice(valeurs)
print(f"J'ai choisi '{ordinateur}'")
