#!/usr/bin/env python3

from pathlib import Path

# Chargement des mots
mots = Path('liste_mots.txt').read_text()
mots = mots.split('\n')

print(f'{len(mots)} ont été chargés')

