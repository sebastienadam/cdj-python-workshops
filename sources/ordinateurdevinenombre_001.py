#!/usr/bin/env python3

valeur_min = 1
valeur_max = 15

print(f'Choisi un nombre entre {valeur_min} et {valeur_max}, je vais le deviner.')
input('Appuie sur "Enter" lorsque tu es prêt')

print()
essais = valeur_min + int((valeur_max - valeur_min) / 2)
print(f"Est-ce que ton nombre est {essais}?")

