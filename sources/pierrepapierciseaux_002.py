#!/usr/bin/env python3

import random

valeurs = ['pierre', 'papier', 'ciseaux']

ordinateur = random.choice(valeurs)
joueur = input(f'Ton choix {valeurs}: ')
print(f"J'ai choisi '{ordinateur}'")
