#!/usr/bin/env python3

import random

nombre_max = 15

print('Devine le nombre')
print()

nombre = random.randint(1, nombre_max)

while True:
    devine = input(f"Ta proposition entre 1 et {nombre_max}: ")
    if devine.isnumeric():
        break
    print('Tu dois choisir un nombre entier')

devine = int(devine)
if nombre == devine:
    print(f'Bravo! tu as bien deviné!')
else:
    print(f"Pas de chance, j'avais choisi {nombre}")
