#!/usr/bin/env python3

valeur_min = 1
valeur_max = 15
reponses_acceptees = ['b', 'c', 'h']

print(f'Choisi un nombre entre {valeur_min} et {valeur_max}, je vais le deviner.')
input('Appuie sur "Enter" lorsque tu es prêt')

while True:
    print()
    essais = valeur_min + int ((valeur_max - valeur_min) / 2)
    print(f"Est-ce que ton nombre est {essais}?")
    print("Si ma proposition est trop haute et que je dois choisir un nombre plus bas, presse 'h'")
    print("Si ma proposition est trop basse et que je dois choisir un nombre plus haut, presse 'b'")
    print("Si c'est le nombre que tu as choisi, presse 'c'")
    while True:
        reponse = input("Quel est ta reponse? ").lower()
        if reponse in reponses_acceptees:
            break
        print(f"Tu dois choisir entre {' - '.join(reponses_acceptees)}")
    if reponse == 'h':
        valeur_max = essais - 1
    elif reponse == 'b':
        valeur_min = essais + 1
    elif reponse == 'c':
        break

print(f"J'ai deviné le nombre, il s'agissait de {essais}")
