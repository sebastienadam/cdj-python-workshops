#!/usr/bin/env python3

from pathlib import Path
from string import ascii_uppercase
import random

# Initialisation du jeu
alphabet = [lettre for lettre in ascii_uppercase]
mots = Path('liste_mots.txt').read_text()
mots = mots.split('\n')

# Initialisation de la partie
mot = random.choice(mots)
lettres_proposees = []
vies = 6

# Affichage du mot à trouver
lettres_affichees = []
for lettre in mot:
    if lettre in lettres_proposees:
        lettres_affichees.append(lettre)
    else:
        lettres_affichees.append('_')
print(' '.join(lettres_affichees))

# Proposition de lettre par le joueur
while True:
    lettre_proposee = input("Ta proposition: ").upper()
    if not lettre_proposee in alphabet:
        print('Tu dois proposer une lettre')
        continue
    if lettre_proposee in lettres_proposees:
        print('Tu as déjà proposé cette lettre')
        continue
    break

# Vérification de la proposition du joueur
if not lettre_proposee in mot:
    print("La lettre n'est pas dans le mot, tu as perdu une vie")
    vies -= 1
lettres_proposees.append(lettre_proposee)
