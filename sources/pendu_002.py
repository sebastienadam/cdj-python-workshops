#!/usr/bin/env python3

from pathlib import Path
import random

# Chargement des mots
mots = Path('liste_mots.txt').read_text()
mots = mots.split('\n')

# Choix du mot
mot = random.choice(mots)

print(f"Le mot choisi est: {mot}")
