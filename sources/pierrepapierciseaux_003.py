#!/usr/bin/env python3

import random

valeurs = ['pierre', 'papier', 'ciseaux']

ordinateur = random.choice(valeurs)
while True:
    joueur = input(f'Ton choix {valeurs}: ').lower()
    if joueur in valeurs:
        break
    print(f'Tu dois choisir entre: {" - ".join(valeurs)}')
print(f"J'ai choisi '{ordinateur}'")
